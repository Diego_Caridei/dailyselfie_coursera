package it.iprog.diegocaridei.dailyselfie;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
/**
 * Created by DiegoCaridei on 18/11/14.
 */
public class BitmapUtility {
    public static Bitmap getBitmap(String bmpPath, int height, int width) {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(bmpPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/width, photoH/height);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(bmpPath, bmOptions);
    }
}
