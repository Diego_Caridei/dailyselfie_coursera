package it.iprog.diegocaridei.dailyselfie;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.ListActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class MainActivity extends ListActivity {
    private static final String tag = "MainActivity";
    static final int REQUEST_TAKE_PHOTO = 1;

    private ArrayList<Selfie> selfies = new ArrayList<Selfie>();
    private SelfieAdapter adapter = null;

    private File storageDir = null;
    private String storagePath = null;
    private String photoPath = null;

    private PendingIntent pendingIntent;
    private AlarmManager alarmManager;

    private ProgressBar progressBar;
    private ListView listView;

    // default thumb sizes
    private int thumbHeight = 300, thumbWidth = 260;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = (ProgressBar) findViewById(R.id.loadProgress);
        listView = (ListView) findViewById(android.R.id.list);


        storageDir = getExternalFilesDir(null);
        storagePath = storageDir.getAbsolutePath();

        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    }


    protected void onResume() {
        super.onResume();

        adapter = new SelfieAdapter(this, R.layout.selfie_row, selfies);
        setListAdapter(adapter);

        // start service from alarm
        Intent serviceIntent = new Intent(this, NotificationService.class);
        pendingIntent = PendingIntent.getService(this, 0, serviceIntent, 0);

        // set the pending intent to fire every 2 mins
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1*60*1000,
                1*60*1000, pendingIntent);

        loadSelfies();
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            dispatchTakePictureIntent();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

            // make a selfie object from the most recently taken photo and add it to collection
            final Selfie newSelfie = new Selfie(new Date(), photoPath);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    selfies.add(newSelfie);
                    updateList();
                }
            });
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timestamp + "_";

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        photoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                // automatically store the file on sdcard after taken
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));

                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void loadSelfies() {
        LoadSelfiesTask task = new LoadSelfiesTask(thumbHeight, thumbWidth);
        task.execute();
    }

    private boolean selfieExists(Selfie s) {
        for (Selfie selfie : selfies) {
            if (selfie.getBitmapPath().equals(s.getBitmapPath())) {
                return true;
            }
        }
        return false;
    }

    private void updateList() {
        Collections.sort(selfies);

        adapter.notifyDataSetChanged();

        changeVisibility(progressBar, View.GONE);
        changeVisibility(listView, View.VISIBLE);

        TextView txt_numSelfies = (TextView) findViewById(R.id.txt_noSelfies);


        /*
        if (selfies.size() > 0) {
            txt_numSelfies.setText("Selfies taken: " + selfies.size());
        } else {
            txt_numSelfies.setText(getResources().getString(R.string.selfie_count));
        }
        */
    }

    private void changeVisibility(final View v, final int visibility) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                v.setVisibility(visibility);
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View view, int position, long id) {
        Selfie clickedSelfie = selfies.get(position);
        String selfiePath = clickedSelfie.getBitmapPath();

        Intent viewIntent = new Intent(this, SelfieViewActivity.class);
        viewIntent.putExtra("bmpPath", selfiePath);

        startActivity(viewIntent);
    }

    // task to read selfies from disk
    class LoadSelfiesTask extends AsyncTask<Void, Void, Void> {
        private final String tag = "LoadSelfiesTask";
        private int height, width;

        public LoadSelfiesTask(int height, int width) {
            this.height = height;
            this.width = width;
        }

        @Override
        protected Void doInBackground(Void... params) {

            changeVisibility(progressBar, View.VISIBLE);

            File storage = new File(storagePath);
            File files[] = storage.listFiles();

            int selfiesLoaded = 0;

            if (files != null) {
                Log.d(tag, "Files found: " + files.length);

                for (File f : files) {
                    if (f.getName().endsWith(".jpg")) {

                        // make a new Selfie add to list
                        Date lastModDate = new Date(f.lastModified());
                        Selfie newSelfie = new Selfie(lastModDate, f.getAbsolutePath());

                        if (!selfieExists(newSelfie)) {
                            newSelfie.loadBmpThumbData(height, width);
                            selfies.add(newSelfie);
                            selfiesLoaded++;
                        }
                    }
                }
            }

            final int numSelfies = selfiesLoaded;

            changeVisibility(progressBar, View.GONE);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateList();
                }
            });

            return null;
        }

    }
}



