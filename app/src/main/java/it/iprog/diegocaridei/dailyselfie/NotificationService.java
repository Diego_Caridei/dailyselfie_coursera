package it.iprog.diegocaridei.dailyselfie;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


public class NotificationService extends Service {
    private static final String tag = "NotificationService";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(tag, "Service created");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(tag, "service onStartCommand()");
        displayNotification();
        return START_NOT_STICKY;
    }

    private void displayNotification() {

        Intent resultIntent = new Intent(this, MainActivity.class);
        final PendingIntent launchAppIntent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Log.d(tag, "Alarm fired at " + System.currentTimeMillis());
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.camera)
                .setContentTitle(getResources().getString(R.string.notification_title))
                .setContentText(getResources().getString(R.string.notification_text))
                .setContentIntent(launchAppIntent);

        int mNotificationId = 1;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = builder.build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        mNotifyMgr.notify(mNotificationId, notification);
    }
}
