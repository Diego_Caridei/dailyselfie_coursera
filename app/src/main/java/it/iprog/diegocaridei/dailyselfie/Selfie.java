package it.iprog.diegocaridei.dailyselfie;

/**
 * Created by DiegoCaridei on 18/11/14.
 */
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by raidzero on 11/16/14.
 */
public class Selfie implements Comparable<Selfie> {
    private static final String tag = "Selfie";

    private Date date = null; // timestamp
    private Bitmap bmpThumb = null;
    private String bmpPath = null; // path to bmp on disk

    public Selfie(Date date, String bmpPath) {
        this.date = date;
        this.bmpPath = bmpPath;
    }

    public void loadBmpThumbData(int height, int width) {
        //Log.d(tag, "loadBmpThumbData(" + height + ", " + width +")");

        bmpThumb = BitmapUtility.getBitmap(bmpPath, height, width);
    }

    public String getDate() {
        return date.toString();
    }

    public Bitmap getThumb() {
        //Log.d(tag, "getThumb() returning " + bmpThumb.getByteCount() + " bytes");

        return bmpThumb;
    }

    public boolean thumbLoaded() {
        //Log.d(tag, "thumbLoaded? " + (bmpThumb != null) + ": " + date);
        return bmpThumb != null;
    }

    public String getBitmapPath() {
        return bmpPath;
    }


    @Override
    public int compareTo(Selfie another) {
        return another.date.compareTo(this.date);
    }
}
