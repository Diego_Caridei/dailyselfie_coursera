package it.iprog.diegocaridei.dailyselfie;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by DiegoCaridei on 18/11/14.
 */
public class SelfieAdapter extends ArrayAdapter<Selfie> {

    private static final String tag = "SelfieAdapter";
    private int layoutResource;

    public SelfieAdapter(Context context, int resource, ArrayList<Selfie> objects) {
        super(context, resource, objects);
        layoutResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Selfie selfie = getItem(position);

        // if we arent reusing a view, inflate one
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(layoutResource, parent, false);
        }

        final ImageView selfieView = (ImageView) convertView.findViewById(R.id.imgView_selfie);
        TextView timestampView = (TextView) convertView.findViewById(R.id.textView_timestamp);

        selfieView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (!selfie.thumbLoaded()) {
                    int height = selfieView.getHeight();
                    int width = selfieView.getWidth();

                    selfie.loadBmpThumbData(height, width);
                }
            }
        });

        if (selfie.thumbLoaded()) {
            selfieView.setImageBitmap(selfie.getThumb());
        }

        timestampView.setText(selfie.getDate());

        return convertView;
    }
}
