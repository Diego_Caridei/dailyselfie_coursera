package it.iprog.diegocaridei.dailyselfie;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ShareActionProvider;

import java.io.File;


public class SelfieViewActivity extends Activity {
    private static final String tag = "SelfieViewActivity";

    private String imagePath = null;
    private ImageView fullScreenView = null;
    private int imageHeight, imageWidth;

    private ShareActionProvider mShareActionProvider;
    private Bitmap selfieBmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        imagePath = intent.getStringExtra("bmpPath");
        setContentView(R.layout.activity_selfie_view);
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        fullScreenView = (ImageView) findViewById(R.id.imgView_largeSelfie);

        imageHeight = fullScreenView.getHeight();
        imageWidth = fullScreenView.getWidth();

        selfieBmp = BitmapUtility.getBitmap(imagePath, imageHeight, imageWidth);
        fullScreenView.setImageBitmap(selfieBmp);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_selfie_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
